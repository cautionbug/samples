# ***Code Samples***

Thank you for taking a look through some of my recent work. Below you'll find several PHP and SQL
samples showing my contributions to an enterprise-level web application.
My work included updating/fixing old code that was often undocumented or difficult to read,
and SQL scripts (MySQL and Oracle) to modify tables and make data corrections.

As for JavaScript, there was very little *new* front-end work during my time on
this application. There were many bugs to fix over the years, but because the
code involved is so ... difficult to follow, I've opted not to display my contributions.

If you have a GitLab account, feel free to leave comments or questions on the snippets.

**One final note:** Whenever possible (particularly new code), i tried to follow best practices.
Legacy design patterns often made this difficult, so the compromise tends to be extensive in-code comments.

# PHP

## Time Entry / Labor Repairs
In this application, Time Entry is simply any recorded work time by a worker, whereas
Labor is worked time applied to a potentially billable project. When a worker records
Time Entry that applies to a project, a separate Labor entry is recorded, which
can produce Charges if the work is billable. Likewise, if a Labor entry is created
from a different part of the system, it creates a corresponding Time Entry record.

For several years, no one seemed to notice that the two features were not connecting
to each other correctly. Then customers began to notice some data not making sense.
After a month of investigating, we finally determined there were major flaws in
the implementation of these two work tracking features.

* Deleting Labor is supposed to delete its Time Entry record, but wasn't.
* Changing aspects of a Time Entry record should be reflected in the Labor, but weren't.
* Labor was being created beyond the cutoff of the project so the corresponding Charges weren't billed.
* Worst of all, modifying Labor in some cases deleted Charges
but didn't indicate it had done so, leaving billable Labor with no Charges.

At the time I left, I had completed work on approximately half of the issues in
the Epic, including some fixes that weren't listed but were knock-on from the fixes
I did make.<br>I've posted that code as before/after branches to show the full picture:

### 7e27ba36

## Archiving
* [Layout Breakup and Refactor]($1893859)<br>
  This was a badly needed separation, and the Archive module was the perfect excuse,
  since it would need its own set of tables and functionality.
* [PIT Archive Layout Models]($1893884)<br>
  Models in the Archive module aren't clones of the original, they capture
  point-in-time data and ETL that data to the Archive version of that table.

## Events
* [Event Error Handling]($1895351)<br>
  For a long time, there was very little ability to trace Event failures because
  Exceptions would get black-holed when run by CLI, and not tell users that a 
  problem occurred because they didn't have access to the Events Grid to check
  on things.<br>
  This stemmed from ongoing complaints about duplicated data, CPU spikes,
  and Event failure reports sent to the Help Desk. We finally diagnosed the
  problem to be Events incorrectly being processed multiple times.<br>
  For my own sanity, I insisted on adding <em>some</em> improvement to error
  handling to reduce our support headaches and wasted time.
* [Events DST infinite loop fix]($1895358)<br>
  Each year in autumn mysterious Event errors and CPU spikes were reported,
  but we couldn't reproduce, so couldn't diagnose. Finally we discovered
  recurring Events were falling into an infinite loop if the schedule put
  them in the 1-hour overlap when Daylight Savings rolls back to Standard Time.<br>
  The original solution for advancing an Event's schedule left out several key
  considerations. Not only would an Event keep getting rescheduled in the
  overlap, but very old Events would cause Fatal errors as the logic tried to
  "catch up" their schedule using a loop.

## Other Significant Changes
A few samples of recent changes that were important to stabilize the application
and enable the last few new features. The existing product was slated to be put
into bugfix-only release cycle before a major rewrite over the next several years.
* [Build Form-Field Key correctly]($1895806)<br>
  As discussed in samples above, the `$formFieldKey` used in Model logic is an
  array typically defined by a Form. In some cases, the Form provides no FFK
  and lets the Model assume that Elements/fields are named in a way the Model
  can handle for building the `INSERT`/`UPDATE` statement. Its original logic
  made fragile and risky assumptions about how to automatically generate a map
  of field names to DB columns.
* Multi-mapping Form Elements<br>
  The two changes below became essential pieces of the repairs to Time Entry
  and Labor tracking. At least 40 individual bug tickets were collected
  together - some of which I had entered in my first few months on the job -
  and I was tasked to diagnose and write fixes for the most urgent 6 issues.
  However, as often happens - especially when repairing legacy code - scope increased
  quickly as issues were codependent and a fix for a scheduled issue overlapped
  the fix for an unscheduled issue so it became necessary to fix both.
    * [`Element::normalizeModelMapping()`]($1893825) (previously `buildModelMapping()`)<br>
      When the Element is constructed it needs to handle various model-map patterns
      that emerged over the years. Their requirements weren't documented and the
      purpose of this function was mostly ambiguous.
    * [Multi-modelMapping Elements]($1893852)<br>
      Now Elements could be mapped to several tables (one column each) without
      needing to be manually mapped to other tables in the Model.

# SQL
* [Labor static rate storage]($1897854)
* [Status Date columns]($1897858)
